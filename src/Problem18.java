import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        int starType;
        int n;
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.print("Please select star type [1-4,5 is Exit]: ");
            starType = sc.nextInt();
            if (starType == 5) {
                System.out.println("Bye bye!!!");
                break;
            }

            switch (starType) {
                case 1:
                    System.out.print("Please input number: ");
                    n = sc.nextInt();
                    for (int row = 0; row < n; row++) {
                        for (int column = 0; column < n; column++) {
                            if (column <= row) {
                                System.out.print("*");
                            } else {
                                System.out.print(" ");
                            }
                        }
                        System.out.println();
                    }
                    break;
                case 2:
                    System.out.print("Please input number: ");
                    n = sc.nextInt();
                    for (int row = 0; row < n; row++) {
                        for (int column = 0; column < n; column++) {
                            if (row <= n - column - 1) {
                                System.out.print("*");
                            } else {
                                System.out.print(" ");
                            }
                        }
                        System.out.println();
                    }
                    break;
                case 3:
                    System.out.print("Please input number: ");
                    n = sc.nextInt();
                    for (int row = 0; row < n; row++) {
                        for (int column = 0; column < n; column++) {
                            if (row <= column) {
                                System.out.print("*");
                            } else {
                                System.out.print(" ");
                            }
                        }
                        System.out.println();
                    }
                    break;
                case 4:
                    System.out.print("Please input number: ");
                    n = sc.nextInt();
                    for (int row = 0; row < n; row++) {
                        for (int column = 0; column <= n - row; column++) {
                            System.out.print(" ");
                        }
                        for (int column = 0; column <= row; column++) {
                            System.out.print("*");
                        }
                        System.out.println();
                    }
                    break;
                default:
                    System.out.println("Error: Please input number between 1-5");
                    break;
            }
        }
        sc.close();
    }
}
