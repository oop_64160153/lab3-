public class Problem7While {
    public static void main(String[] args) {
        int row = 0;
        while (row < 5) {
            int column = 0;

            while (column < 5) {
                System.out.print(column);
                column++;
            }
            System.out.println();
            row++;
        }
    }
}
