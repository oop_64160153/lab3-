public class Problem17 {
    public static void main(String[] args) {
        for (int row = 1; row <= 5; row++) {
            int count = 1;
            for (int column = 1; column <= 5 - row; column++) {
                System.out.print(" ");
                count++;
            }
            for (int column = 1; column <= row; column++) {
                System.out.print(count);
                count++;
            }
            System.out.println();
        }
    }
}
