import java.util.Scanner;

public class Problem5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String txt;
        while (true) {
            System.out.print("Please input : ");
            txt = sc.nextLine();
            if (txt.equalsIgnoreCase("bye")) {
                System.out.println("Exit Program");
                break;
            }

            System.out.println(txt);
        }
        sc.close();
    }
}
