public class Problem8While {
    public static void main(String[] args) {
        int row = 1;
        while (row <= 5) {
            int column = 1;

            while (column <= 5) {
                System.out.print(column);
                column++;
            }
            System.out.println();
            row++;
        }
    }
}
