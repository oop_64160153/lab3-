import java.util.Scanner;

public class Problem9For {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input n: ");
        int n = sc.nextInt();
        for (int row = 1; row <= n; row++) {
            for (int column = 1; column <= n; column++) {
                System.out.print(column);
            }
            System.out.println();
        }
        sc.close();
    }
}
