import java.util.Scanner;

public class Problem3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input first number: ");
        String strNum1 = sc.next();
        int num1 = Integer.parseInt(strNum1);
        System.out.print("Please input second number: ");
        String strNum2 = sc.next();
        int num2 = Integer.parseInt(strNum2);

        if (num1 <= num2) {
            for (int i = num1; i <= num2; i++) {
                System.out.print(i + " ");
            }
        } else {
            System.out.println("Error");
        }
        sc.close();
    }
}
