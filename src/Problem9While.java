import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input n: ");
        int n = sc.nextInt();
        int row = 1;
        while (row <= n) {
            int column = 1;

            while (column <= n) {
                System.out.print(column);
                column++;
            }
            System.out.println();
            row++;
        }
        sc.close();
    }
}
